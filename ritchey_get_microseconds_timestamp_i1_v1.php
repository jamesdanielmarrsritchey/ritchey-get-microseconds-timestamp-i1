<?php
#Name:Ritchey Get Microseconds Timestamp i1 v1
#Description:Get timestamp in microseconds. Returns timestamp as an interger on success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values.
#Arguments:'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):display_errors:bool:optional
#Content:
if (function_exists('ritchey_get_microseconds_timestamp_i1_v1') === FALSE){
function ritchey_get_microseconds_timestamp_i1_v1($display_errors = NULL){
	$errors = array();
	if ($display_errors === NULL){
		$display_errors = FALSE;
	} else if ($display_errors === TRUE){
		#Do Nothing
	} else if ($display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task []
	if (@empty($errors) === TRUE){
		$microseconds_timestamp = @microtime();
		$microseconds_timestamp = @explode(' ', $microseconds_timestamp);
		###Convert microseconds (which is currently in seconds as a decimal) to microseconds rounded to the nearest microsecond.
		$microseconds_timestamp[0] = $microseconds_timestamp[0] * 1000000;	
		$microseconds_timestamp[0] = @intval(@round($microseconds_timestamp[0]));
		###Convert seconds to microseconds
		$microseconds_timestamp[1] = @intval($microseconds_timestamp[1]);
		$microseconds_timestamp[1] = $microseconds_timestamp[1] * 1000000;
		$microseconds_timestamp = $microseconds_timestamp[0] + $microseconds_timestamp[1];
		if (@is_int($microseconds_timestamp) !== TRUE){
			$errors[] = 'microseconds_timestamp';
		}
	}
	result:
	##Display Errors
	if ($display_errors === TRUE){
		if (@empty($errors) === FALSE){
			$message = @implode(", ", $errors);
			if (function_exists('ritchey_get_microseconds_timestamp_i1_v1_format_error') === FALSE){
				function ritchey_get_microseconds_timestamp_i1_v1_format_error($errno, $errstr){
					echo $errstr;
				}
			}
			set_error_handler("ritchey_get_microseconds_timestamp_i1_v1_format_error");
			trigger_error($message, E_USER_ERROR);
		}
	}
	##Return
	if (@empty($errors) === TRUE){
		return $microseconds_timestamp;
	} else {
		return FALSE;
	}
}
}
?>